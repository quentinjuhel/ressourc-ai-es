# Ressourc(AI)e
[FR]
Ce repo vise à documenter une critique des recherches et de la démocratisation récente des AI et de l'automatisation numérique.

[ENG]
This repo document a criticism about the recent research and democratisation of the AIs and digital automation.


## De la question écologique <br />Of the ecological question

- "Carbon Emissions and Large Neural Network", Google, University of Californie, Berkeley,  [https://arxiv.org/pdf/2104.10350.pdf](https://arxiv.org/pdf/2104.10350.pdf)
[ecology]

## De la question en art et design <br/>Of the art and design question

- Étienne Mineur, "Réflexions provisoire liées aux intelligence artificielles", in blog, etienne.desgin. [https://etienne.design/2022/05/16/ai/](https://etienne.design/2022/05/16/ai/) 
[design][education]

-  Étienne Mineur, "Intelligence artificielles et design dans les écoles en 2022", in blog, etienne.desgin. [https://etienne.design/2022/09/27/ai-2/](https://etienne.design/2022/09/27/ai-2/)

- Olivier Ertzscheid," Une question de génération. Vers un capitalisme sémiotique. ",  affordance.info, 12 octobre 2022. [https://affordance.framasoft.org/2022/10/question-generation-capitalisme-semiotique/](https://affordance.framasoft.org/2022/10/question-generation-capitalisme-semiotique/)
[image]

- Gœffrey Dorne, "Comment j’ai intégré l’intelligence artificielle dans mon travail de graphiste.", in graphism.fr. [https://graphism.fr/comment-jai-integre-lintelligence-artificielle-dans-mon-travail-de-graphiste-ia/](https://graphism.fr/comment-jai-integre-lintelligence-artificielle-dans-mon-travail-de-graphiste-ia/)
[design]

- Reject design, "Dalle-2 & AI Design Tools", [https://reject.design/index.php/2022/08/01/ai-design-tools/](https://reject.design/index.php/2022/08/01/ai-design-tools/)[design]

- La cellule, "La génération d'images par des IA va-t-ell bouleverser le monde l'illustation ?", podcast. [https://open.spotify.com/episode/5gJgkuNmYgrAAOvGe5rERd?si=ce87727641ef43d4&nd=1](https://open.spotify.com/episode/5gJgkuNmYgrAAOvGe5rERd?si=ce87727641ef43d4&nd=1) [illustration] [JDR]

## De la question du développement web <br /> Of web programation question

- "Sketching Interfaces, Generating code from low fidelity wireframes",
Benjamin Wilkins, Behind the Scenes, airbnb.design. [https://airbnb.design/sketching-interfaces/](https://airbnb.design/sketching-interfaces/) 
[design][code]

## De la question de la musique <br /> Of music question 

- Rdiffusion, using stable diffusion AI to generate spectogram. [https://www.riffusion.com/about](https://www.riffusion.com/about) [music][stablediffusion]
- Coditany of Timeness, dadabots, black metal AI generated album [https://dadabots.bandcamp.com/album/coditany-of-timeness](https://dadabots.bandcamp.com/album/coditany-of-timeness)
- Dadabots, generation music with AI. [https://dadabots.com/science.php](https://dadabots.com/science.php)